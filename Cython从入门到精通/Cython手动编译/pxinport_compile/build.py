import sys
sys.path.append('.')

import pyximport
# 这里同样指定 language_level=3, 则表示针对的是py3, 因为这种方式也是要编译的
pyximport.install(language_level=3)
# 执行完之后, Python 解释器在导包的时候就会识别 Cython 文件了, 当然会先进行编译

import add
print(add.add(111, 22))
