cdef extern from "../build_cython_c/cfib.h":
    double cfib(int n)

def fib_with_c(n):
    return cfib(n)
