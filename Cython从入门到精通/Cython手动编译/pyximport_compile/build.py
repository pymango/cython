import sys
sys.path.append('.')

import pyximport
pyximport.install(language_level=3)

import fib
print(fib.fib_with_c(50))