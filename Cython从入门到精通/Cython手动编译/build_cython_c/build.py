from distutils.core import setup, Extension
from Cython.Build import cythonize

# 通过 Extension 对象的方式可以实现更多功能
# 指定的 name 表示编译之后的文件名, 编译之后会得到 cfib.cp38-win_amd64.pyd
# sources 代表源文件, 只需要指定 pyx 和 c 源文件
# include_dirs 指定头文件的所在目录
ext = Extension(name="fib", sources=["fib.pyx", "cfib.c"])
setup(ext_modules=cythonize(ext))
